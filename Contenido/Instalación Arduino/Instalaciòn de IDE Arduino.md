# Instalaciòn de IDE Arduino 

## Paso 1 

**1.1** Ingresar a la siguiente: **[URL](https://www.arduino.cc/)**

<img src="https://i.ibb.co/q921Rt1/ARDUINO-1.png" width=400>

**1.2** Luego es necesario dirigirse a la opción de software

<img src="https://i.ibb.co/6ZVyxBd/ARDUINO-2.png" width=400>

**1.3** Después seleccionar en el panel izquierdo la opción de Linux 64 Bits, al seleccionarlo nos aparecerá si deseamos colaborar con Arduino, para este caso solo vamos a descargar

<img src="https://i.ibb.co/mT47sqH/ARDUINO-3.png" width=400>

**1.4** Nos aparecerá la ventana de abrir el compresor para lo cual se pasará al escritorio y se extraerá la carpeta

<img src="https://i.ibb.co/GprPgcX/ARDUINO-4.png" width=400>

## Paso 2

`Una vez descargado extraemos la carpeta y nos aparecerá los siguientes archivos`

<img src="https://i.ibb.co/0tmw5bj/ARDUINO-5.png" width=400>

**2.1** Ingresamos a la carpeta de Examples / Blind, con click derecho permitirá abrir una
terminal desde la zona indicada, para lo cual escribiremos los siguientes comandos: 

```
cd arduino-1.8.19/
/Escritorio/arduino-1.8.19$ ls -l
```

`De esta manera se iniciará la instalación del Arduino, Nos va a solicitar la contraseña, la vamos a ingresar y a continuar con la instalación:`

<img src="https://i.ibb.co/gyQjhDT/ARDUINO-6.png" width=400>

**Nota:** Si al momento de ejecutar el comando presenta error
“Ocurre debido a que no cuenta con permisos de instalación”

<img src="https://i.ibb.co/3BYMFQq/ARDUINO-7.png" width=400>

`Para solucionarlo debes ejecutar el comando de la siguiente manera y asi finalizar la instalaciòn:`
```
sudo ./install.sh
```
<img src="https://i.ibb.co/nD1ZFLv/ARDUINO-8.png" width=400>

`Esto permitirá la instalación del Arduino, para cambiar el acceso directo en el escritorio se debe ejecutar el siguiente comando`

```
sudo chown $(whoami):$(whoami) /home/$(whoami)/Escritorio/arduinoarduinoide.desktop
```

## Paso 3

Ahora se debe conectar el Arduino al Computador es importante aclarar que la
conexión se debe realizar primero al Arduino y luego por el cable usb al computador.

<img src="https://i.ibb.co/QbNV095/ARDUINO-9.png" width=400>

## Paso 4

Si se realizó todo de manera satisfactoria ir a la carpeta de instalación Arduino1.8.19-linux64 y seguir la ruta hasta la carpeta BLINK,

<img src="https://i.ibb.co/4Wn6R5L/ARDUINO-10.png" width=400>

## Paso 5

Dentro de la carpeta blink, ejecutar el archivo Blink.ino

<img src="https://i.ibb.co/km2z6KG/ARDUINO-11.png" width=400>

## Paso 6

Antes de iniciar cualquier practica es muy importante validar el tipo de Placa que se va
a utilizar y le puerto

* **Placa**

<img src="https://i.ibb.co/frv2PbZ/ARDUINO-13.png" width=400>

* **Puerto**

<img src="https://i.ibb.co/J7LM6QH/ARDUINO-14.png" width=400>

## Paso 7

Tenemos dos botones importantes que son Verificar y Subir con los cuales ejecutaremos los
comandos aplicados sobre la Placa

* **Verificar**

<img src="https://i.ibb.co/wRZ6Gn9/ARDUINO-15.png" width=400>

* **Subir**

<img src="https://i.ibb.co/fk8RX10/ARDUINO-16.png" width=400>

`Si al momento de ejecutar se presenta el siguiente error:`

<img src="https://i.ibb.co/YZrm9gQ/ARDUINO-17.png" width=400>

`Se deben habilitar los puertos desde la consola con el siguiente comando:`

```
sudo chmod 777 /dev/ttyACM0
```

<img src="https://i.ibb.co/YfC6nmv/ARDUINO-18.png" width=400>

`Luego continuamos con la ejecución y se podrá evidenciar el siguiente mensaje de conectividad, y en el Arduino el LED de acuerdo con el delay configurado.`

<img src="https://i.ibb.co/9bkhgz7/ARDUINO-19.png" width=400>

# <a href="https://gitlab.com/semillero-nov-2022/semillero-2022/-/tree/main"> REGRESAR</a>
