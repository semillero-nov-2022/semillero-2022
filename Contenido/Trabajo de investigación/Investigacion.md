# Que es un Encoder

Sensor para conocer la posición angular y normalmente se encuentran acoplados a motores.
Proporciona mediciones y controles precisos sobre la velocidad del motor, la velocidad lineal, el
posicionamiento angular o lineal.

El encoder es responsable de contar o reproducir a partir del movimiento rotacional de su eje,
convirtiendo los movimientos rotacionales o desplazamientos lineales en impulsos eléctricos de onda
cuadrada o senoidal, generando una cantidad de pulsos por rotación precisa a lo largo de su rotación.
El Enconder consta de un eje rotatorio, unido a un disco que posee partes transparentes y opacas, y un
emisor de luz infrarroja. El emisor de luz infrarroja emite una luz que es interceptada por el disco con
partes transparentes y opacas, que a su vez gira con el movimiento del eje rotatorio.
Esto hace que la luz pase o sea bloqueada por las partes opacas del disco y forme una secuencia la cual
es transformada en señal digital a través de un sensor óptico o foto-receptor. Esto hace posible el
control del movimiento, posición y en algunas aplicaciones velocidad.
Existen dos tipos de Enconder según la forma de establecer la posición. Estos son:
Enconder de detección incremental: estos dispositivos detectan el movimiento y miden la distancia
en función de la diferencia entre dos valores establecidos primeramente.
Enconder de detección absoluta: Se toma como cero la posición inicial del objeto y se mide de forma
absoluta a partir de esta posición.

# Que es un Motor y para que funciona
Es la parte sistemática de una maquina cuyo propósito principal es brindar la energía suficiente a un
conjunto de piezas para que estas tengan un funcionamiento adecuado y la máquina que componen
pueda realizar sus actividades.

# Nema 23

El motor paso a paso es un motor Nema 23 bipolar de corriente continua sin escobillas en el que la rotación se divide en un cierto número de pasos resultantes de la estructura del motor. Normalmente, una
revolución completa del eje de 360° se divide en 200 pasos, lo que significa que se realiza una sola carrera del eje cada 1,8°.

# Que es RVIZ

Es un visualizador gráfico que nos permite la representación en 3D de entornos para robots, sensores y
algoritmos. Es fácilmente configurable para cualquier tipología de robot y nos permite de forma
sencilla mostrar en pantalla o en un entorno gráfico un considerable número de variables de ROS.
también nos ofrece un gran número de plugin y paneles que son configurables y modificables para
satisfacer la función que queramos realizar con él.

# Glosario:

### ROS: 

Es un sistema operativo flexible que provee librerías y herramientas para ayudar a los
desarrolladores de software a crear aplicaciones para robots.

### Fuente; 

Unidad que suministra energía eléctrica a otro componente de una maquina

### Arduino:

Es una plataforma de hardware libre, basada en una placa con un microcontrolador y un
entorno de desarrollo diseñada para el uso de la electrónica en proyecto

### Controlador: 

Se constituye por un microordenador con unidad central encargado de calcular los procesos, movimientos y comandos a ejecutar.

### Microcontrolador; 

Es un circuito integrado programable que contiene todos los componentes de un
computador dispone de un procesador, memoria para programas y los datos, línea de entrada y salida
de datos puede controlar cualquier cosa y suele estar incluido en el mismo dispositivo que controla

### Coordenadas: 

Sistema de ejes para el posicionamiento de un punto en el plano o en el espacio pueden
ser;

➢ Angulares, si la referencia de un punto se hace mediante ángulos a partir de los ejes(origen de
los ángulos).
➢ Polares, Se establece un punto mediante la indicación de un ángulo y un valor escalar
(numérico).
➢ Rectangulares, cuando los puntos están definidos por varios números (dos o tres)
### Eje: 

Cada una de las lineas según las cuales puede moverse el brazo robótico o una parte de el.

### Encoders: 

Sensor para conocer la posición angular y normalmente se encuentran acoplados a motores.

### Motor: 

Se define como maquina destinada a producir movimiento a expensas de otra fuente de energía

### Servomotor; 

Es un actuador rotativo o motor que permite un control preciso en términos de posición
angular, aceleración y velocidad, capacidades que un motor normal no tiene. En definitiva, utiliza un
motor normal y lo combina con un sensor para la retroalimentación de posición.

### Motor Paso a Paso: 

Motor que se controla mediante una serie de pulsos eléctricos, cada vez que el
motor recibe un pulso gira en un ángulo fijo, este ángulo es lo que se llama paso del motor.

➢ Ventaja: es posible controlar exactamente la posición de su eje
➢ Desventaja: alto costo y baja potencia

Nota: Son motores sumamente precisos

### Librerías de Programación: 

conjunto de archivos de computador que agregan nuevas capacidades a un lenguaje de computación especifico.

### Python: 

Es un lenguaje de programación de alto nivel utilizado para desarrollar diversas aplicaciones.
A diferencia de otros lenguajes como Java o .NET, es un lenguaje interpretado, es decir, no necesita ser
compilado para ejecutar aplicaciones escritas en Python, sino que es ejecutado directamente por la
computadora mediante un programa llamado intérprete, por lo que no hay necesidad de "traducirlo" a
lenguaje de máquina.

### Rotación: 

Movimiento básico en un manipulador. 
