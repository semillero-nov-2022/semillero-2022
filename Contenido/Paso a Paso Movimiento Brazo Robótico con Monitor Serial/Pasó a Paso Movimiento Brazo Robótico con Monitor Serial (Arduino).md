# Pasó a Paso Movimiento Brazo Robótico con Monitor Serial (Arduino)

## Paso 1 

Abrimos Arduino IDE

<img src="https://i.ibb.co/xFrMh0m/Arduino-M-S1.png" width=400>

## Paso 2

Incluiremos la librería Servo.h que se encarga de facilitarnos el manejo de
servomotores con nuestro arduino

```
#include <Servo.h> // Libreria
```
<img src="https://i.ibb.co/9HhhhWc/Arduino-M-S2.png" width=400>

## Paso 3

Ahora declararemos cada articulación de nuestro Brazo Robotico utilizando el parámetro Servo antes del nombre de la articulación

### 3.1 Para un Brazo Robótico de 4 Articulaciones quedaría de la siguiente forma

```
#include <Servo.h>  // Libreria

Servo rotacion;  // declaramos variables dependiendo de las articulaciones
Servo derecho;
Servo izquierdo;
Servo pinza;
```
<img src="https://i.ibb.co/w6JpwQR/Arduino-M-S3.png" width=400>

## Paso 4

Ahora declaramos un ángulo inicial para nuestro servomotor

```
#include <Servo.h>  // Libreria
Servo rotacion;  // declaramos variables dependiendo de las articulaciones
Servo derecho;
Servo izquierdo;
Servo pinza;
int angulorotacion=90; // Declaramos el angulo inicial para el servomotor
int anguloderecho=90;
int anguloizquierdo=90;
int angulopinza=90;    // Solo hasta 130
```
<img src="https://i.ibb.co/HdgZwqG/Arduino-M-S4.png" width=400>

## Paso 5

Procedemos a configurar nuestra placa

### 5.1 Serial.begin(9600); le indica a nuestro Arduino que inicie comunicación con la computadora (o
cualquier dispositivo conectado a los pines RX y TX) con una velocidad de comunicación serial
de 9600 bits por segundo (baudios)

```
void setup(){

rotacion.attach(2)  // Se asocia pin 2 (dependiendo de la conexion del arduino)
derecho.attach(6)
izquiero.attach(5)
pinza.attach(3)
Serial.begin(9600); // Iniciamos el puerto serial con una velocidad de comunicacion 9600 bits por segundo
```
<img src="https://i.ibb.co/Q8yFK0j/Arduino-M-S5.png" width=400>

## Paso 6

Procedemos a crear nuestro void loop, presentaremos el código a continuación y
explicaremos por cada línea con un comentario.

```
void loop(){

unsigned char comando=0; //Datos de 8 bits conteniendo un rango de 0 a 255
if(Serial.available()){//solo leeremos si hay un byte en el buffer
comando=Serial.read();//leemos el byte
if(comando=='a'){
angulorotacion+=30;//incrementamos 10
rotacion.write(angulorotacion); // escribimos el nuevo angulo en nueestro servomotor
Serial.println(angulorotacion); imprimimos el angulo actual en nuestro monitor serial
}
else if(comando=='s'){
angulorotacion-=30;//decrementamos 10
rotacion.write(angulorotacion);
Serial.println(angulorotacion);
}
}
}//End loop

```
<img src="https://i.ibb.co/80PbT8n/Arduino-M-S6.png" width=400>

## Paso 7

Para finalizar encontraremos el código completo a continuación

```
#include <Servo.h> // Libreria
Servo rotacion; // Declarar variables dependiendo de las articulaciones del brazo
Servo derecho;
Servo izquierdo;
Servo pinza;
int angulorotacion=90; // Se declara un angulo inicial para el servomotor
int anguloderecho=90;
int anguloizquierdo=90;
int angulopinza=90; // Solo hasta 130
void setup() {

rotacion.attach(2); // Se asocia el Pin 3 (dependiendo de la conexion en el arduino)
derecho.attach(6);
izquierdo.attach(5);
pinza.attach(3);
Serial.begin(9600); // Iniciamos el puerto serial con con una velocidad de comunicación serial de
9600 bits por segundo (baudios)
}
void loop() {
unsigned char comando=0;
 if(Serial.available()){
 comando=Serial.read();
 if(comando=='a'){
 angulorotacion+=10; //incrementamos 30
 rotacion.write(angulorotacion); //Escribimos el nuevo angulo en nuestro servomotor
 Serial.println(angulorotacion); //imprimos el angulo actual en nuestro monitor serial
 }
 else if (comando=='s'){
 angulorotacion-=10; //decrementamos 10
 rotacion.write(angulorotacion); //Escribimos el nuevo angulo en nuestro servomotor
 Serial.println(angulorotacion); //imprimos el angulo actual en nuestro monitor serial
 }
 else if (comando=='w'){
 anguloderecho+=10; //incrementamos 30
 derecho.write(anguloderecho); //Escribimos el nuevo angulo en nuestro servomotor
 Serial.println(anguloderecho); //imprimos el angulo actual en nuestro monitor serial
 }
 else if (comando=='r'){
 anguloderecho-=10; //incrementamos 30
 derecho.write(anguloderecho); //Escribimos el nuevo angulo en nuestro servomotor
 Serial.println(anguloderecho); //imprimos el angulo actual en nuestro monitor serial
 }
 else if (comando=='l'){
 anguloizquierdo+=10; //incrementamos 30
 izquierdo.write(anguloizquierdo); //Escribimos el nuevo angulo en nuestro servomotor
 Serial.println(anguloizquierdo); //imprimos el angulo actual en nuestro monitor serial
 }
 else if (comando=='k'){
 anguloizquierdo-=10; //incrementamos 30
 izquierdo.write(anguloizquierdo); //Escribimos el nuevo angulo en nuestro servomotor
 Serial.println(anguloizquierdo); //imprimos el angulo actual en nuestro monitor serial
 }
 else if (comando=='b'){
 angulopinza=90; //incrementamos 30
 pinza.write(angulopinza); //Escribimos el nuevo angulo en nuestro servomotor
 Serial.println(angulopinza); //imprimos el angulo actual en nuestro monitor serial
 }
 else if (comando=='n'){
 angulopinza=130; //incrementamos 30
 pinza.write(angulopinza); //Escribimos el nuevo angulo en nuestro servomotor
 Serial.println(angulopinza); //imprimos el angulo actual en nuestro monitor serial
 }
 }
 }
```

## Paso 8

Ahora vamos a ejecutar movimientos con la opción o herramienta monitor serial es el‘cable’ entre el ordenador y el Arduino UNO. Permite enviar y recibir mensajes de texto.

`¿Cómo abrir nuestro monitor serial?`

**1.** Damos click en Herramientas – Monitor Serie.

**2.** Podremos observar

<img src="https://i.ibb.co/QpfD2t8/Arduino-M-S7.png" width=400>

* Una configuración de 115200 baudios.
* Ingreso de comandos.

<img src="https://i.ibb.co/PZT7FM2/Arduino-M-S8.png" width=400>

# <a href="https://gitlab.com/semillero-nov-2022/semillero-2022/-/tree/main"> REGRESAR</a>
