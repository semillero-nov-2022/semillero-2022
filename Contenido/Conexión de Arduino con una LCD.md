## Paso 1

De acuerdo a la práctica de laboratorio realizada el sábado 18 día de junio ejecutamos esta conexión con base a la información que se relaciona en esta **[URL](https://naylampmechatronics.com/blog/34_tutorial-lcd-conectando-tu-arduino-a-un-lcd1602-y-lcd2004.html)**

<img src="https://i.ibb.co/kykgq7M/LCD1.jpg" width=400>

## Paso 1.1: 

Reconocemos los componentes que vamos a utilizar como los son:

<img src="https://i.ibb.co/Qj2nJRj/LCD2.png" width=300 height = 300>
<img src="https://i.ibb.co/PF8MFqM/LCD3.png" width=300 height = 300>

## Paso 2: 

Ingresamos a nuestro PC Abrimos Arduino IDE

## Paso 2.1:

Validamos la configuracion y ajustes que requerimos para ejecutar la conexion

<img src="https://i.ibb.co/1MpzW6M/LCD4.png" width=500 height = 300>

* Nos dirigimos al panel de arduino y seleccionamos **HERRAMIENTAS** despues **PLACA ARDUINO UNO** y por ultimo opcion **ARDUINO UNO**

<img src="https://i.ibb.co/1TGws9t/LCD5.png" width=300 height = 300>


## Paso 3: 

En otra ventana antes de ejecutar el código debemos identificar qué tipo de LCD se está manejando para ello corremos el siguiente código de monito serie con base a la información que se relaciona en esta **[URL](https://www.ardumotive.com/i2clcden.html)**

```
// Escáner I2C 
// Escrito por Nick Gammon 
// Fecha: 20 de abril de 2011

#include <Cable.h>

configuración vacía () {
  Serial.begin (9600);
  Serial.println ( "Escáner I2C. Escaneo..." );
  recuento de bytes = 0;
  
  Alambre.begin();
  para (byte i = 8; i < 120; i++)
  {
    Wire.beginTransmission (i);
dieciséis if (Cable.endTransmission () == 0)
      {
      Serial.print ( "Dirección encontrada: " );
      Serial.print (i, DEC);
      Serial.print ( "(0x" );
      Serial.print (i, HEX);
      Serial.println( ")" );
      contar++;
      retraso (1);  // tal vez innecesario? 
      } // fin de la buena respuesta 
  } // fin del ciclo for 
  Serial.println ( "Terminado." );
  Serial.print ( "Encontrado" );
  Serial.print (recuento, DEC);
  Serial.println ( "dispositivo(s)." );
}   // fin de la instalación

bucle vacío () {}

```
* Una vez ejecutado arrojara el siguiente resultado dependiendo del tipo de LCD

<img src="https://i.ibb.co/7W1k8Qs/LCD8.png" width=600 height = 300>

## Paso 4:

Una ves inditificado el tipo de LCD lo remplazamos en en codigo del paso 3 

```
LiquidCrystal_I2C lcd(0x27,16,2);
``` 
<img src="https://i.ibb.co/Wf9rL02/LCD9.png" width=600 height = 300>

## Paso 5:

Una vez abierto el Arduino copiamos y pegamos el código del link del paso 1  

```

#include <Wire.h> 
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x3F,16,2);  //Validar que tipo de  lcd  se está utilizando

void setup() {   

lcd.init();                 // Inicializar el LCD

lcd.backlight();           //Encender la luz de fondo.

lcd.print("Hola Mundo");  // Escribimos el Mensaje en el LCD.
}

void loop() {            

lcd.setCursor(0, 1);      // Ubicamos el cursor en la primera posición(columna:0) de la segunda línea(fila:1)

cd.print(millis()/1000); // Escribimos el número de segundos trascurridos
lcd.print(" Segundos");
delay(100);
}

```

<img src="https://i.ibb.co/kxxtjKV/LCD6.png" width=600 height = 300>

* Despues de ingresar el codigo verificamos en el siguiente icocno que todo este bien

<img src="https://i.ibb.co/r7SQQXw/LCD11.png" width=30 height = 30>

* Una vez verificamos seleccionamos el siguiente icono para mostrar los datos en la LCD

<img src="https://i.ibb.co/5WHWmLf/LCD12.png" width=30 height = 30>

**Resultado: una vez ejecutados todos los pasos debería arrojar lo siguiente**

<img src="https://i.ibb.co/FgJP15L/LCD10.png" width=300 height = 300>


# <a href="https://gitlab.com/semillero-nov-2022/semillero-2022/-/tree/main"> REGRESAR</a>
