Se han realizado investigaciones técnicas y de conocimiento para abarcar los lineamientos técnicos del proyecto y las aplicaciones que vamos a utilizar para llevar a cabo el desarrollo del proyecto. En las sesiones que hemos llevado a cabo desde el  inicio del proyecto hemos realizado las configuraciones de los programos que se requieren para el desarrollo y configuraciòn de nuestro entorno de trabajo; como avances significativos hemos realizado la instalaciòn y configuraciòn de  Ubuntu 20.04, ROS Noetic, configuraciòn de nuestra WorkSpace y la clonación de los paquetes, instalaciòn de catkin.
Hemos realizado la investigaciòn de conocimiento para identificar cada  uno de los componentes que se integra con  el desarrollo del proyecto tales como :

### ¿Qué es Ros?

Ros se puede entender como un sistema operativo, sin embargo es una colección de frameworks que permiten desarrollar un software para robots, un framework es un espacio de trabajo que sirve como plantilla o como base para la elaboración de una tarea o actividad, por lo tanto podemos decir que Ros es un conjunto de plantillas o bases que al programarlas y unirlas correctamente, se puede implementar un software para un robot determinado que cumpla con las tareas a realizar.

* Robotic Operating System (ROS)

– Es un ambiente de desarrollo para robótica que promueve la re-utilización de componentes.

– Es un conjunto de bibliotecas y herramientas para el desarrollo de aplicaciones robóticas.

* Nace a partir de la ausencia de estándares para la robótica.

* Historia:

Es una iniciativa del Stanford Artificial Intelligence Laboratory (2007) y su desarrollo fue continuado por Willow Garage

### ¿Que es un Encoder?

Sensor para conocer la posición angular y normalmente se encuentran acoplados a motores. Proporciona mediciones y controles precisos sobre la velocidad del motor, la velocidad lineal, el posicionamiento angular o lineal.

El encoder es responsable de contar o reproducir a partir del movimiento rotacional de su eje, convirtiendo los movimientos rotacionales o desplazamientos lineales en impulsos eléctricos de onda cuadrada o senoidal, generando una cantidad de pulsos por rotación precisa a lo largo de su rotación.

El Enconder consta de un eje rotatorio, unido a un disco que posee partes transparentes y opacas, y un emisor de luz infrarroja. Se toma como cero la posición inicial del objeto y se mide de forma absoluta a partir de esta posición.

### ¿Que es un Motor y para que funciona?

Es la parte sistemática de una maquina cuyo propósito principal es brindar la energía suficiente a un
conjunto de piezas para que estas tengan un funcionamiento adecuado y la máquina que componen
pueda realizar sus actividades.

**Nema 23**

Un NEMA 23 es motor paso a paso bipolar de corriente continua sin escobillas en el que la rotación se divide en un cierto número de pasos resultantes de la estructura del motor. Normalmente, una
revolución completa del eje de 360° se divide en 200 pasos, lo que significa que se realiza una sola carrera del eje cada 1,8°.

<img src="https://i.ibb.co/W0BLXM4/Motor-23.png" width=400>

### ¿Que es RVIZ?

Es un visualizador gráfico fácilmente configurable para cualquier topología de robot y nos permite de forma
sencilla mostrar en pantalla o en un entorno gráfico un considerable número de variables de ROS.

* Proporciona visualización 3D de sensores y robots (URDF).
* Permite visualizar la información en un sistema de coordenadas común

### ¿Qué es Ros Launch?

* Herramienta para levantar varios nodos y setear parámetros.
* roslaunch opera sobre un archivo launch (XML).
* roslaunch automáticamente ejecuta roscore si es necesario.

# Glosario:

### ROS:

Es un sistema operativo flexible que provee librerías y herramientas para ayudar a los desarrolladores de software a crear aplicaciones para robots.

### Roscore:

roscore es el primer comando a ejecutar cuando se comienza a utilizar ROS.

### Fuente:

Unidad que suministra energía eléctrica a otro componente de una maquina

### Arduino:

Es una plataforma de hardware libre, basada en una placa con un microcontrolador y un entorno de desarrollo diseñada para el uso de la electrónica en proyecto

### Controlador:

Se constituye por un microordenador con unidad central encargado de calcular los procesos, movimientos y comandos a ejecutar.

### Microcontrolador:

Es un circuito integrado programable que contiene todos los componentes de un
computador dispone de un procesador, memoria para programas y los datos, línea de entrada y salida
de datos puede controlar cualquier cosa y suele estar incluido en el mismo dispositivo que controla

### Coordenadas:

Sistema de ejes para el posicionamiento de un punto en el plano o en el espacio pueden ser;

* Angulares, si la referencia de un punto se hace mediante ángulos a partir de los ejes (origen de los ángulos).
* Polares, Se establece un punto mediante la indicación de un ángulo y un valor escalar (numérico).
* Rectangulares, cuando los puntos están definidos por varios números (dos o tres)

### Eje:

Cada una de las lineas según las cuales puede moverse el brazo robótico o una parte de el.

### Encoders:

Sensor para conocer la posición angular y normalmente se encuentran acoplados a motores.

### Motor:

Se define como maquina destinada a producir movimiento a expensas de otra fuente de energía

### Servomotor:

Es un actuador rotativo o motor que permite un control preciso en términos de posición
angular, aceleración y velocidad, capacidades que un motor normal no tiene. En definitiva, utiliza un
motor normal y lo combina con un sensor para la retroalimentación de posición.

### Motor Paso a Paso;

Motor que se controla mediante una serie de pulsos eléctricos, cada vez que el motor recibe un pulso gira en un ángulo fijo, este ángulo es lo que se llama paso del motor.

* Ventaja: es posible controlar exactamente la posición de su eje
* Desventaja: alto costo y baja potencia

**Nota:** Son motores sumamente precisos

### Librerías de Programación:

Conjunto de archivos de computador que agregan nuevas capacidades a un lenguaje de computación especifico.

### Python:

Es un lenguaje de programación de alto nivel utilizado para desarrollar diversas aplicaciones.
A diferencia de otros lenguajes como Java o .NET, es un lenguaje interpretado, es decir, no necesita ser
compilado para ejecutar aplicaciones escritas en Python, sino que es ejecutado directamente por la
computadora mediante un programa llamado intérprete, por lo que no hay necesidad de "traducirlo" a
lenguaje de máquina.

### Rotación:

Movimiento básico en un manipulador que representa la rotación de θ grados del plano en sentido antihorario.


# <a href="https://gitlab.com/semillero-nov-2022/semillero-2022/-/tree/main"> REGRESAR</a>
