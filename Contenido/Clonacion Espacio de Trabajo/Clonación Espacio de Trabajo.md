# Clonación Espacio de Trabajo

## Paso 1: 

* Abrimos una terminal con **ctrl** + **alt** +**T**

## Paso 2:

* Ingresamos a nuestro espacio de trabajo con el siguiente codigo y oprimimos enter

```
cd ws_moveit/
```
* Se debe corre el roscore dentro del espacio de trabajo con el siguiente comando

```
roscore
```
![Clonacion1](https://i.ibb.co/FhBCHCY/Clonacion1.png)

**NOTA:** `En otra ventana, sin cerrar en la que acabamos de iniciar ROS, ejecutamos el siguiente comando dentro del espacio de trabajo donde se van actualizar los paquetes`

## Paso 3:

* Una Vez estemos dentro de nuestro espacion de trabajo ejecutamos el siguiente comando y Enter

```
cd src

```

## Paso 4:

* Dentro de la terminal realizamos la clonación de los paquetes con el comando

```
git clone https://github.com/bandasaikrishna/ros_control_example.git
```

![Clonacion8](https://i.ibb.co/pdwSDYq/Clonacion-8.png)

* Luego damos **cd** Enter para salir del espacio de trabajo

```
cd
```

## Paso 5: 

* Luego de clonar los paquetes, se debe construir → catkin build desde nuestro espacio de trabajo con los siguientes comandos

```
cd ws_moveit/
catkin build
```
![Clonacion2](https://i.ibb.co/v1g9B13/Clonacion-2.png)

## Paso 6:

* Realizamos la actualización de paquetes con el siguiente comando

```
/ws_moveit$ source devel/setup.bash  // Actualizar paquetes
```
![Clonacion3](https://i.ibb.co/R3TBY5Z/Clonacion-3.png)

**NOTA** En otra ventana, sin cerrar en la que estamos trabajando, ejecutamos el siguiente comando 

## Paso 7:

* Una vez actualizados los paquetes realizamos el lanzamiento de cada uno:

### 7.1 Primero lanzaremos los .launch del paquete control_example, importante estar dentro el espacio de trabajo en el source

```
~/ws_moveit$/src/ros_control_example/ros_control_example/launch$   // Enter 
roslaunch ros_control_example check_velocity_controller.launch     // Enter escojemos el paquete que vamos a lanzar de los que se mencionan abajo de este codigo   
```
* check_position_controller.launch
* check_urdf.launch
* check_velocity_controller.launch

![Clonacion4](https://i.ibb.co/Jv9jHD3/Clonacion-4.png)

### 7.2 Segundo lanzaremos los .launch del paquete 3-DOF, importante estar dentro el espacio de trabajo en el source

```
~/ws_moveit$/src/3-DOF_Manipulator/three_dof_planar_manipulator/launch/   // Enter 
roslaunch three_dof_planar_manipulator check_motor_controls.launch        // Enter escojemos el paquete que vamos a lanzar de los que se mencionan abajo de este codigo   
```
* check_motor_controls.launch
* check_urdf.launch

![Clonacion6](https://i.ibb.co/3sN2wJs/Clonacion-6.png)

### 7.3 Tercero lanzaremos los .launch del paquete 6-DOF, importante estar dentro el espacio de trabajo en el source


```
~/ws_moveit$/src/6-DOF_Manipulator/six_dof_spatial_manipulator/launch/   // Enter 
roslaunch six_dof_spatial_manipulator check_motor_controls.launch        // Enter escojemos el paquete que vamos a lanzar de los que se mencionan abajo de este codigo

```

* check_motor_controls.launch
* check_urdf.launch

![Clonacion5](https://i.ibb.co/b5b1RKq/Clonacion-5.png)

**NOTA**  Se puede presentar el siguiente error, para resolverlo a continuación se dejan los pasos para su solución: 

* Si salen errores se debe ingresar al .launch
* En la línea 10 identificar donde dice **xacro.py**  y quitar el **.py**

![Clonacion7](https://i.ibb.co/mq6knqQ/Clonacion-7.png)

# <a href="https://gitlab.com/semillero-nov-2022/semillero-2022/-/tree/main"> REGRESAR</a>
