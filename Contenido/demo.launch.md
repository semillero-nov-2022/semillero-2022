#  Lanzamiento demo.launch

## Paso 1: 

* Abrimos una terminal con **ctrl** + **alt** +**T**

## Paso 2:

* Ingresamos a nuestro espacio de trabajo y ejecutamos los siguientes códigos y por cada oprimimos Enter

```
cd ws_moveit/
```
```
source devel/setup.bash
```
```
roscore
```
<img src="https://i.ibb.co/thSpPZ2/demo-launch.png" width=500 height = 250>

**NOTA:** `En otra ventana, sin cerrar en la que acabamos de iniciar ROS, ejecutamos el siguiente comando dentro del espacio como se muestra en la imagen`


```
roslaunch six_dof_dof_spatial_manipulator_moveit_conig demo.launch

```

<img src="https://i.ibb.co/zbNyddS/demo-launch1.png" width=500 height = 250>

* Una vez ejecutado se desplegaran varios procesos que durante 2 min.. y cuando finalice se abrirá la siguiente imagen

<img src="https://i.ibb.co/jvGSRSy/demo-launch3.png" width=500 height = 250>

## Paso 3:

* Podremos visualizar los movimientos del brazo en tiempo real

<img src="https://i.ibb.co/0KN7GH2/demo-launch4.png" width=500 height = 250>

# <a href="https://gitlab.com/semillero-nov-2022/semillero-2022/-/tree/main"> REGRESAR</a>
