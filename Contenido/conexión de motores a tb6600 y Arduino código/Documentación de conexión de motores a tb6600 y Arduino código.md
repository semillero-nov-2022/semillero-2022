# Documentación de conexión de motores a tb6600 y Arduino código 


## Paso 1 

Reconocemos los componentes que vamos a utilizar como los son:

* 1.1 Motor Pasó a Paso Bipolar el cual cuenta con dos bobinas formando 4 polos variables, los cuales forman un par por cada boina.

<img src="https://i.ibb.co/8bG0rWg/Motor-P-P1.png" width=350 height = 350>

* 1.2 Controlador TB6600 en el cual se realizan las conexiones de la parte de control

<img src="https://i.ibb.co/qpw8488/Motor-P-P2.png" width=400 height = 250>

* 1.3 Arduino: En el arduino se realiza la conexión de los pines segun el codigo a cargar para que se ejecute el movimiento del motor

<img src="https://i.ibb.co/xJtvTh2/Motor-P-P3.png" width=400 height = 250>

<img src="https://i.ibb.co/6wDj5Y5/Motor-P-P4.png" width=400 height = 250>

* 1.4 Fuente de Alimentación la cual suministra electricidad, en este caso se tomó 9V

<img src="https://i.ibb.co/vYQn7rd/Motor-P-P5.png" width=400 height = 350>

## Paso 2

Vamos a utilizar un multímetro para validar continuidad y poder determinar las
conexiones del motor. El motor trae cuatro cables la mayor parte de veces esto
viene estandarizado ya que el negro y verde es de una bobina y el rojo y azul de
la otra.

## Paso 3

Conexiones

* 3.1 Para controlar nuestro motor paso a paso vamos a utilizar el controlador, vamos a conectar los bobinados (Ay B) y la alimentación (GND-VCC)

<img src="https://i.ibb.co/kqqYKcn/Motor-P-P6.png" width=400 height = 250>
<img src="https://i.ibb.co/2ScgKC7/Motor-P-P7.png" width=400 height = 250>

* 3.2 En esta parte van las conexiones de las señales de control ENA- ENA+ donde permite la conexión anodo y catodo común.

<img src="https://i.ibb.co/NsvzmFp/Motor-P-P8.png" width=400 height = 250>

* 3.4 DIR y PUL, en DIR siempre utilizaremos una para la dirección y PUL es para las señales de pulsos para que cada vez que se active un pulso el motor gire un paso


## Paso 4

Debemos revisar los Micro Sweet tiene 6 ya que según la configuración que setiene validamos la tabla cada uno de los valores que indican la corriente que se puede suministrar.

<img src="https://i.ibb.co/pfWP6GD/Motor-P-P9.png" width=400 height = 250>


## Paso 5

Realizamos las conexiones de los componentes de la siguiente forma debe quedarnos: Esquema de conexión ÁNODO Común

<img src="https://i.ibb.co/Cmhh78d/Motor-P-P1013.png" width=400 height = 250>

<img src="https://i.ibb.co/C28HGSN/Motor-P-P11.png" width=400 height = 250>

<img src="https://i.ibb.co/tpDCvDZ/Motor-P-P1213.png" width=400 height = 250>

<img src="https://i.ibb.co/HNhmJQ1/Motor-P-P13.png" width=400 height = 250>

## Paso 6

Para finalizar encontraremos el código completo a continuación

```
int PUL=7; //Pin para la señal de pulso
int DIR=6; //define Direction pin
int EN=5; //define Enable Pin
void setup() {
 pinMode (PUL, OUTPUT);
 pinMode (DIR, OUTPUT);
 pinMode (EN, OUTPUT);
 digitalWrite(EN,HIGH);
}
void loop() {
 digitalWrite(DIR,LOW);
 for (int i=0; i<1600; i++) //Forward 1600 steps
 {
 digitalWrite(PUL,HIGH);
 delayMicroseconds(400);
 digitalWrite(PUL,LOW);
 delayMicroseconds(400);
 }
 delay(100);
 digitalWrite(DIR,HIGH);
 for (int i=0; i<1600; i++) //Backward 1600 steps
 {
 digitalWrite(PUL,HIGH);
 delayMicroseconds(400);
 digitalWrite(PUL,LOW);
 delayMicroseconds(400);
 }
}
```
# <a href="https://gitlab.com/semillero-nov-2022/semillero-2022/-/tree/main"> REGRESAR</a>
