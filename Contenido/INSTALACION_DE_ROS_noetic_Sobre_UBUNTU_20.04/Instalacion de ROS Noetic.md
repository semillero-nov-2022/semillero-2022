# INSTALACION DE ROS Noetic

## Paso 1 

Ingresaremos al siguiente Link **[ROS](https://wiki.ros.org/noetic/Installation)**

<img src="https://i.ibb.co/fGxjZJt/ROS-1.png" width=400>

**1.1** Seleccionaremos Ubuntu

<img src="https://i.ibb.co/D71nVCR/ROS-2.png" width=400>

## Paso 2

Procederemos a instalar algunos repositorios de Ubuntu y llaves.

**2.1** Abriremos una terminal usando la combinación de teclas **Ctrl + Alt + T** y dentro pegaremos las siguientes líneas, para instalar fuentes y llaves respectivas

```
sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" >
/etc/apt/sources.list.d/ros-latest.list'
```

* Configuraremos la lista de fuentes.

**NOTA** : `Nos solicitara el password que tengamos como administrador`

<img src="https://i.ibb.co/3SfmJGv/ROS-3.png" width=400>

**2.2** Configuraremos las llaves

```
sudo apt install curl
```

<img src="https://i.ibb.co/fd7hHx1/ROS-4.png" width=400>

**NOTA** `Nos preguntara si deseamos continuar y le daremos “Y”, dejamos que corran las librerias y una vez finalice ejecutamos el siguiente comando`

<img src="https://i.ibb.co/FgQD9CZ/ROS-5.png" width=400>

```
curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | sudo apt-key add -
```
<img src="https://i.ibb.co/SPg2SJR/ROS-6.png" width=400>

**2.3** Actualizo los paquetes de Ubuntu

```
sudo apt update
```
<img src="https://i.ibb.co/qyr13jT/ROS-7.png" width=400>

**NOTA** `Es recomendable utilizar la instalación de ros noetic desktop Full cuando se usa en una Pc para que de esta manera se instalen la mayoría de paquetes y dependencias usadas comúnmente.`

```
sudo apt install ros-noetic-desktop-full
```
<img src="https://i.ibb.co/1mBvWk2/ROS-8.png" width=400>

Nota: `Nos preguntara si deseamos continuar y le daremos **“Y”**` 

<img src="https://i.ibb.co/N7rHdJS/ROS-9.png" width=400>

**2.4** Una vez instalado Ros procedemos a crear el ambiente, de esta manera se agrega la ruta noetic al archivo. bashrc

```
echo "source /opt/ros/noetic/setup.bash" >> ~/.bashrc
source ~/.bashrc
```
<img src="https://i.ibb.co/rZB3C7W/ROS-10.png" width=400>

**2.5** pasaremos a revisar los archivos creados en nuestra carpeta, una vez allí utilizaremos la combinación de teclas **Ctrl + H** para visualizar los archivos ocultos

<img src="https://i.ibb.co/BzpFLWM/ROS-11.png" width=400>

**2.6** Procedemos a instalar dependencia para compilar paquetes

```
sudo apt install python3-rosdep python3-rosinstall python3-rosinstall-generator python3-
wstool build-essential
```

<img src="https://i.ibb.co/nBFydXx/ROS-12.png" width=400>

**NOTA**: `Nos preguntara si deseamos continuar y le daremos “Y”`

<img src="https://i.ibb.co/X8qSW2p/ROS-13.png" width=400>

**2.6.1** Para hacer uso de muchas de las herramientas de Ros, deberemos inicializar
rosdep, de esta manera nos permitirá instalar fácilmente las dependencias delsistema.

```
sudo apt install python3-rosdep
```
<img src="https://i.ibb.co/Bqwr66H/ROS-14.png" width=400>

Actualizo con la siguiente ejecución

```
sudo rosdep init
rosdep update
```

<img src="https://i.ibb.co/CW3FcTd/ROS-15.png" width=400>

## Paso 3

**3.1** Para verificar la correcta instalación de Ros deberemos ejecutar la siguiente línea
sobre la terminal y que se observe como se ilustra acontinuación.

```
roscore
```

<img src="https://i.ibb.co/936g9Cy/ROS-16.png" width=400>

**3.2** para verificar la distribución de Rosinstalada.

```
rosversion -d
```
<img src="https://i.ibb.co/zRkthZX/ROS-17.png" width=400>

# <a href="https://gitlab.com/semillero-nov-2022/semillero-2022/-/tree/main"> REGRESAR</a>
