#  Bienvenidos

# Tabla de Contenido

## 1. Introduccion

El presente proyecto se consiste en desarrollar y programar los movimientos precisos de un brazo Robótico mediante controladores que permitan soportar una carga no mayor a 3 LB  en el sector  de procesos industriales definiendo una empresa para su implementación, esta misma será ubicada en la ciudad de Bogotá.

A continuación, se encontrarán detalladamente aspectos importantes como los requerimientos del sistema, las aplicaciones y las herramientas a utilizar para el correcto manejo del brazo robótico y los roles que tienen acceso al manejo del mismo. 
 

Documentación de contextos generales se puede encontrar **[Aquí.](https://gitlab.com/semillero-nov-2022/semillero-2022/-/blob/main/Contenido/Introducci%C3%B3n.md)**


## 2. Organigrama

En el siguiente **ORGANIGRAMA**  representamos de manera visual la estructura y organizacion en la cual nos basaremos para desarrollar el proyecto

Diagrama de caso  de uso se puede encontrar **[Aquí.](https://gitlab.com/semillero-nov-2022/semillero-2022/-/blob/main/Contenido/Caso%20de%20Uso.md)**


<img src="https://i.ibb.co/mSmsKSH/Esquema.png" width=600>

* Proceso a seguir y poner en practica durante el semillero

<img src="https://i.ibb.co/QY3XCXb/Organigrama.png" width=600>


## 3. ROS

A continuación, encontrara una guía práctica paso a paso para realizar la Instalación de **ROS Noetic en UBUNTU
20.04:**

Documentación **[Aquí.](https://gitlab.com/semillero-nov-2022/semillero-2022/-/blob/main/Contenido/INSTALACION_DE_ROS_noetic_Sobre_UBUNTU_20.04/Instalacion%20de%20ROS%20Noetic.md)**

## 4. Clonacion de Paquetes

A continuación, encontrara una guía práctica paso a paso para realizar para realizar la **Clonación en un Espacio de Trabajo.**

Documentación **[Aquí.](https://gitlab.com/semillero-nov-2022/semillero-2022/-/blob/main/Contenido/Clonacion%20Espacio%20de%20Trabajo/Clonaci%C3%B3n%20Espacio%20de%20Trabajo.md)**

## 5. Arduino

Arduino es una plataforma de desarrollo basada en una placa electrónica de hardware libre
que incorpora un microcontrolador reprogramable y una serie de pines hembra.
En esta guía podrá encontrar como realizar la instalación de Arduino sobre el sistema operativo
Ubuntu 20.04

Documentación **[Aquí.](https://gitlab.com/semillero-nov-2022/semillero-2022/-/blob/main/Contenido/Instalaci%C3%B3n%20Arduino/Instalaci%C3%B2n%20de%20IDE%20Arduino.md)**

## 6. Paso a Paso Movimiento Brazo Robotico


A continuación, encontrara una guía práctica par realizar paso a paso el **Movimiento de un Brazo Robótico con Monitor Serial.**

Documentación **[Aquí.](https://gitlab.com/semillero-nov-2022/semillero-2022/-/blob/main/Contenido/Paso%20a%20Paso%20Movimiento%20Brazo%20Rob%C3%B3tico%20con%20Monitor%20Serial/Pas%C3%B3%20a%20Paso%20Movimiento%20Brazo%20Rob%C3%B3tico%20con%20Monitor%20Serial%20(Arduino).md)**

## 7. Esquema conexion a Arduino con Motor

De acuerdo a la práctica de laboratorio realizada el dia Martes 26 Abril, se investiga la conexión de un motor paso a paso con el controlador TB6600 y Arduino Uno, con base al video que a continuación se relaciona **https://www.youtube.com/watch?v=31vf504h7d4**

Documentación **[Aquí.](https://gitlab.com/semillero-nov-2022/semillero-2022/-/blob/main/Contenido/conexi%C3%B3n%20de%20motores%20a%20tb6600%20y%20Arduino%20c%C3%B3digo/Documentaci%C3%B3n%20de%20conexi%C3%B3n%20de%20motores%20a%20tb6600%20y%20Arduino%20c%C3%B3digo.md)**

<img src="https://i.ibb.co/vP2Bp4C/Esquema-Electronico-de-conexi-n-Arduino-a-Motores.png" width=400 height = 400>
<img src="https://i.ibb.co/wcQdZ8m/Whats-App-Image-2022-04-29-at-8-11-07-PM.jpg" width=400 height = 400>
<img src="https://i.ibb.co/2vbZdNj/Whats-App-Image-2022-04-29-at-8-11-26-PM.jpg" width=400 height = 400>
<img src="https://i.ibb.co/YtnRQRL/Whats-App-Image-2022-04-29-at-8-11-55-PM.jpg" width=400 height = 400>

## 8. Conexión de Arduino con  una LCD 

En esta guía explicaremos el funcionamiento de una LCD conectada al Arduino ya que para el proyecto necesitamos visualizar y monitorear los parámetros para hacer testeo de los posible valores que se entregan en las variables.

Documentación **[Aquí.](https://gitlab.com/semillero-nov-2022/semillero-2022/-/blob/main/Contenido/Conexi%C3%B3n%20de%20Arduino%20con%20una%20LCD.md)**

## 9. Lanzamiento demo.launch

A continuación, encontrara una guía práctica par realizar paso a paso el **Lanzamiento demo.launch**

Documentación **[Aquí.](https://gitlab.com/semillero-nov-2022/semillero-2022/-/blob/main/Contenido/demo.launch.md)**

## 10. Codigo de testeo Movimiento

```
#include <ros.h>
#include <rospy_tutorials/Floats.h>
#include <std_msgs/String.h>
#include <Wire.h> 
#include <AccelStepper.h>
#include <MultiStepper.h>

// Joint 1
#define E1_STEP_PIN        0 
#define E1_DIR_PIN         1

// Joint 2
#define Z_STEP_PIN         2
#define Z_DIR_PIN          3


// Joint 3
#define Y_STEP_PIN         4
#define Y_DIR_PIN          5

// Joint 4
#define X_STEP_PIN         6
#define X_DIR_PIN          7


// Joint 5 
#define E0_STEP_PIN        8
#define E0_DIR_PIN         9

// Joint 6

#define E2_STEP_PIN         10
#define E2_DIR_PIN          11


// ------------------- DECLARACIÓN ACCELSTEPPER POR CADA MOTOR, MANEJA DOS PIN --------


AccelStepper joint1(AccelStepper::FULL2WIRE, E1_STEP_PIN, E1_DIR_PIN);// Variables asociadas a los pin de direccion y pulso
AccelStepper joint2(AccelStepper::FULL2WIRE, Z_STEP_PIN, Z_DIR_PIN);// Variables asociadas a los pin de direccion y pulso
AccelStepper joint3(AccelStepper::FULL2WIRE, Y_STEP_PIN, Y_DIR_PIN);// Variables asociadas a los pin de direccion y pulso
AccelStepper joint4(AccelStepper::FULL2WIRE, X_STEP_PIN, X_DIR_PIN);// Variables asociadas a los pin de direccion y pulso
AccelStepper joint5(AccelStepper::FULL2WIRE, E0_STEP_PIN, E0_DIR_PIN);// Variables asociadas a los pin de direccion y pulso
AccelStepper joint6(AccelStepper::FULL2WIRE, E2_STEP_PIN, E2_DIR_PIN);// para cada motor

MultiStepper steppers;

// ------------------- DEFINICIÓN DE PIN PARA FINALES DE CARRERA --------

#define home_switch 23 // Pin  connected to Home Switch (MicroSwitch)
#define home_switch2 22 // Pin  connected to Home Switch (MicroSwitch)
#define home_switch3 21 // Pin  connected to Home Switch (MicroSwitch)
#define home_switch4 20 // Pin  connected to Home Switch (MicroSwitch)
#define home_switch5 17 // Pin  connected to Home Switch (MicroSwitch)
#define home_switch6 16 // Pin  connected to Home Switch (MicroSwitch)

long initial_homing=-1;

// ------------------- Variables Y Inicio de ROS ---------------

ros::NodeHandle  nh;
long joint_step[6];


// ------------------- CONEXION CON ROS ---------------

void servo_cb( const rospy_tutorials::Floats& cmd_msg){
  //nh.loginfo("Conexion establecida ROS-ARDUINO ");

  joint_step[0] = (cmd_msg.data[0]);
  joint_step[1] = (cmd_msg.data[1]);
  joint_step[2] = (cmd_msg.data[2]);
  joint_step[3] = (cmd_msg.data[3]);
  joint_step[4] = (cmd_msg.data[4]);
  joint_step[5] = (cmd_msg.data[5]);

}

ros::Subscriber<rospy_tutorials::Floats> sub("/joints_to_aurdino",servo_cb);

// ------------------- EJECUCIÓN DE SETUP ---------------


void setup() {

//--------------- NODOS ROS --------------- 

  nh.initNode();
  nh.subscribe(sub);
  
//--------------- ENTRADA LIMIT-SWTICH ---------------   

   pinMode(home_switch, INPUT_PULLUP); // ENTRADA
   pinMode(home_switch2, INPUT_PULLUP);// ENTRADA
   pinMode(home_switch3, INPUT_PULLUP); // ENTRADA
   pinMode(home_switch4, INPUT_PULLUP); // ENTRADA 
   pinMode(home_switch5, INPUT_PULLUP); // ENTRADA  
   pinMode(home_switch6, INPUT_PULLUP); // ENTRADA

   delay(5);  // Espere a que EasyDriver se despierte


  //Joint1 
  joint1.setMaxSpeed(5000); //10.000(movimientos conjuntos)     //Establezca la velocidad máxima de pasos (más lenta para obtener una mejor precisión)velocidad 10000
  joint1.setAcceleration(500);  // Establecer aceleración de paso a paso velocidad 1000
  joint1.setSpeed(500);
 
  //Joint2
  
  joint2.setMaxSpeed(4000);      //Establezca la velocidad máxima de pasos (más lenta para obtener una mejor precisión)
  joint2.setAcceleration(800);  // Establecer aceleración de paso a paso 
  joint2.setSpeed(200);


  //Joint3
  
  joint3.setMaxSpeed(13000);      //Establezca la velocidad máxima de pasos (más lenta para obtener una mejor precisión)
  joint3.setAcceleration(8000);  // Establecer aceleración de paso a paso 
  joint3.setSpeed(800);

    //Joint4

  joint4.setMaxSpeed(20000);      //Establezca la velocidad máxima de pasos (más lenta para obtener una mejor precisión)
  joint4.setAcceleration(15000);  // Establecer aceleración de paso a paso 
  joint4.setSpeed(5000);
  delay(5);

    //Joint5

  joint5.setMaxSpeed(25000);      //Establezca la velocidad máxima de pasos (más lenta para obtener una mejor precisión)
  joint5.setAcceleration(20000);  // Establecer aceleración de paso a paso 
  joint5.setSpeed(10000);
  delay(5);

  //Joint6
  
  joint6.setMaxSpeed(10000);      //Establezca la velocidad máxima de pasos (más lenta para obtener una mejor precisión)
  joint6.setAcceleration(20000);  // Establecer aceleración de paso a paso 
  joint6.setSpeed(1000);


 
// ------------ INICIA EL PROCEDIMIENTO DE REFERENCIA DEL MOTOR PASO A PASO EN EL ARRANQUE ------------

  //Joint6 Arranque
 
  while (digitalRead(home_switch6) !=1) { // Haga que el Stepper se mueva hacia la izquierda hasta que se active el interruptor negado
    joint6.moveTo(initial_homing);  // Establece la posición a la que moverte
    initial_homing++;  // Establecer direccion del movimiento
    joint6.run();  // Empezar a mover el paso a paso
    }
    delay(1000); //son 1000 milisegundos en un segundo 
    
    joint6.setCurrentPosition(0);  // Establecer la posición actual como cero por ahora
    joint6.setMaxSpeed(20000);      // Establezca la velocidad máxima de pasos (más lenta para obtener una mejor precisión)
    joint6.setAcceleration(10000);  // Establecer aceleración de paso a paso
    joint6.setSpeed(2000);
    delay(1000); //son 1000 milisegundos en un segundo
     
   // segunda comprobación


    do {
    joint6.moveTo(initial_homing);  // Establece la posición a la que moverte
    initial_homing++;  // Establecer direccion del movimiento
    joint6.run();  // Empezar a mover el paso a paso
    
    } while (digitalRead(home_switch6) !=1);
    joint6.setCurrentPosition(0);  // Establecer la posición actual como cero por ahora
    joint6.setMaxSpeed(20000);      // Establezca la velocidad máxima de pasos (más lenta para obtener una mejor precisión)
    joint6.setAcceleration(10000);  // Establecer aceleración de paso a paso
    joint6.setSpeed(2000);
    delay(1000); //son 1000 milisegundos en un segundo


   //Joint5 Arranque
 
while (digitalRead(home_switch5) != 1) { // Haga que el Stepper se mueva hacia la izquierda hasta que se active el interruptor negado
    joint5.moveTo(initial_homing);  // Establece la posición a la que moverte
    initial_homing++;  // Establecer direccion del movimiento
    joint5.run();  // Empezar a mover el paso a paso
    }
    delay(1000); //son 1000 milisegundos en un segundo 
    
    joint5.setCurrentPosition(0);  // Establecer la posición actual como cero por ahora
    joint5.setMaxSpeed(50000);      // Establezca la velocidad máxima de pasos (más lenta para obtener una mejor precisión)
    joint5.setAcceleration(40000);  // Establecer aceleración de paso a paso
    joint5.setSpeed(7500);
    delay(1000); //son 1000 milisegundos en un segundo

    
   // segunda comprobación


    do {
    joint5.moveTo(initial_homing);  // Establece la posición a la que moverte
    initial_homing++;  // Establecer direccion del movimiento
    joint5.run();  // Empezar a mover el paso a paso
    
    } while(digitalRead(home_switch5) !=1 );
    joint5.setCurrentPosition(0);  // Establecer la posición actual como cero por ahora
    joint5.setMaxSpeed(50000);      // Establezca la velocidad máxima de pasos (más lenta para obtener una mejor precisión)
    joint5.setAcceleration(40000);  // Establecer aceleración de paso a paso
    joint5.setSpeed(7500);
    initial_homing=-1; //Inicializar variable negativa para el siguiente movimiento
    delay(1000); //son 1000 milisegundos en un segundo


  //Joint1 Arranque

    
  while (digitalRead(home_switch)) {  // Haga que el Stepper se mueva hacia la izquierda hasta que se active el interruptor
    joint1.moveTo(initial_homing);  // Establece la posición a la que moverte
    initial_homing--;  // Establecer direccion del movimiento
    joint1.run();  // Empezar a mover el paso a paso
    }
    delay(1000); //son 1000 milisegundos en un segundo 
    
    joint1.setCurrentPosition(0);  // Establecer la posición actual como cero por ahora
    joint1.setMaxSpeed(5000);   // Establezca la velocidad máxima de pasos (más lenta para obtener una mejor precisión)
    joint1.setAcceleration(3000); // Establecer aceleración de paso a paso
    joint1.setSpeed(400);
    initial_homing=-1; //Inicializar variable negativa para el siguiente movimiento
    delay(1000); //son 1000 milisegundos en un segundo 
    
  //Joint2 Arranque
  
   while (!digitalRead(home_switch2)) {  //!-> Signo de admiración antes de Digital significa negado  // Stepper se mueva hacia la izquierda hasta que se active el interruptor negado
    joint2.moveTo(initial_homing);  // Establece la posición a la que moverte
    initial_homing--;  // Establecer direccion del movimiento
    joint2.run();  // Empezar a mover el paso a paso
    }
    delay(1000); //son 1000 milisegundos en un segundo 
    
    joint2.setCurrentPosition(0);  // Establecer la posición actual como cero por ahora
    joint2.setMaxSpeed(7000);      // Establezca la velocidad máxima de pasos (más lenta para obtener una mejor precisión)
    joint2.setAcceleration(4000);  // Establecer aceleración de paso a paso
    joint2.setSpeed(400);  
    initial_homing=-1; //Inicializar variable negativa para el siguiente movimiento
    delay(1000); //son 1000 milisegundos en un segundo 
    
  //Joint3 Arranque
  
  while (!digitalRead(home_switch3)) { // Haga que el Stepper se mueva hacia la izquierda hasta que se active el interruptor negado
    joint3.moveTo(initial_homing);  // Establece la posición a la que moverte
    initial_homing++;  // Establecer direccion del movimiento
    joint3.run();  // Empezar a mover el paso a paso
    }
    delay(1000); //son 1000 milisegundos en un segundo 
    
    joint3.setCurrentPosition(0);  // Establecer la posición actual como cero por ahora
    joint3.setMaxSpeed(10000);      // Establezca la velocidad máxima de pasos (más lenta para obtener una mejor precisión)
    joint3.setAcceleration(5000);  // Establecer aceleración de paso a paso
    joint3.setSpeed(500);
    delay(1000); //son 1000 milisegundos en un segundo

  //Joint4 Arranque

  while (!digitalRead(home_switch4)) { // Haga que el Stepper se mueva hacia la izquierda hasta que se active el interruptor negado
    joint4.moveTo(initial_homing);  // Establece la posición a la que moverte
    initial_homing++;  // Establecer direccion del movimiento
    joint4.run();  // Empezar a mover el paso a paso
    }
    delay(1000); //son 1000 milisegundos en un segundo 
    joint4.setCurrentPosition(0);  // Establecer la posición actual como cero por ahora
    joint4.setMaxSpeed(50000);      // Establezca la velocidad máxima de pasos (más lenta para obtener una mejor precisión)
    joint4.setAcceleration(40000);  // Establecer aceleración de paso a paso
    joint4.setSpeed(7500);
    delay(1000); //son 1000 milisegundos en un segundo
 

    // Then give them to MultiStepper to manage
    
  steppers.addStepper(joint1);
  steppers.addStepper(joint2);
  steppers.addStepper(joint3);
  steppers.addStepper(joint4);
  steppers.addStepper(joint5);
  steppers.addStepper(joint6);
  
// ------------ Condicional Para comprobar OK 1 o 0 ------------

      if (digitalRead(home_switch) == 0 && digitalRead(home_switch2) == 1 && digitalRead(home_switch3) == 1
      && digitalRead(home_switch4)== 1 && digitalRead(home_switch5) == 1 && digitalRead(home_switch6) == 1){ // Con este al final se devuelven al tiempo

        //Funciones Debug
        joint_step[0]= 0; 
        joint_step[1]= 0;  
        joint_step[2]= 0; //90°//variable estatica
        joint_step[3]= 0; //90°
        joint_step[4]= 0; //90°
        joint_step[5]= 0; //90°
        steppers.moveTo(joint_step);
        steppers.runSpeedToPosition();
    
      }
}

void loop() {

    long positions[6];  // La matriz de posiciones deseadas del motor paso a paso debe ser larga
    positions[0] = (((joint_step[0])*80000)/180); // negated since the real robot rotates in the opposite direction as ROS
    positions[1] = (((joint_step[1])*20000)/90);
    positions[2] = -(((joint_step[2])*66000)/90);//variable dinamica
    positions[3] = -(((joint_step[3])*70000)/90);
    positions[4] = -(((joint_step[4])*32000)/180); //para realizar pruebas de testeo se deja el primero en 0 y se remplaza los demas por por valores de devolucion asignado
    positions[5] = -(((joint_step[5])*42000)/90); 

    steppers.moveTo(positions);
    steppers.runSpeedToPosition(); 
    nh.spinOnce();
}


## 11. Glosario


* **pinMode(pin, mode):** Esta instruccion es utilizadaq en la parte de la configuracion setup() y sirve para configurar el modo de tabajo de un PIN pudiendo ser INPUT (entrada) u OUTPUT(salida) las terminales de Arduino, por defecto estan configurados como entradas

* **digitalWrite(pin, value):** Envia al pin definido previamente como OUTPUT el valor HIGH o LOW (poniendo en 1 o 0 la salida). El pin puede especificar ya sea como una variable o una constante.

* **digitalRead (pin):** Lee el valor de un pin(definido como digital) dando un resultado HIGH(alto) o LOW (bajo). El pin se puede especificar ya sea como una variable o una consatante

* **void move (long relativa):** esta instruccion es utilizada para establecer la posicion objetivo desde la posicion actual

* **void moveTo (long absoluta):** esta instruccion establece la posicion objetivo

* **boolean run():** Activa el motor para que actúe. Es necesario indicar una velocidad y aceleración previa a declarar esta función para que actue.

* **boolean runSpeed():** moverá el motor a la velocidad que se indicará llamando a la función setSpeed().

* **void setMaxSpeed (float velocidad):** establece la velocidad maxima permitida.Junto con la
función run() acelerará el motor hasta llegar a la velocidad maxima, si no se indica nada más.
Float será el tipo de dato.

* **void setAcceleration (float aceleración):** establece la tasa de aceleración/desaceleración.

* **long distanceToGo():** distancia que queda entre la posición actual y la objetivo.

* **long targetPosition():** establece la posición objetivo.

```
